# comics to rss

Verwende die öffentlichen Informationen von https://mangaread.org/ und erstelle einen RSS-feed um einen bestimmten Comic zu folgen.

RSS-feeds:
- [MangaNelo - Tales Of Demons And Gods](https://neroburner.gitlab.io/comics_to_rss/tales-of-demons-and-gods.xml) - [WebSite](https://manganelo.com/manga/hyer5231574354229)
- [MangaNelo - My Wife Is A Demon Queen](https://neroburner.gitlab.io/comics_to_rss/my-wife-is-a-demon-queen.xml) - [WebSite](https://manganelo.com/manga/wp918498)
- [MangaRead - the-rebirth-of-the-demon-god](https://neroburner.gitlab.io/comics_to_rss/the-rebirth-of-the-demon-god.xml) - [WebSite](https://www.mangaread.org/manga/the-rebirth-of-the-demon-god/)
- [MangaNelo - Solo Leveling](https://neroburner.gitlab.io/comics_to_rss/solo-leveling.xml) - [WebSite](https://manganelo.com/manga/pn918005)
- [MangaRead - auto-hunting](https://neroburner.gitlab.io/comics_to_rss/auto-hunting.xml) - [WebSite](https://www.mangaread.org/manga/auto-hunting/)
- [MangaNelo - villager](https://neroburner.gitlab.io/comics_to_rss/villager.xml) - [WebSite](https://manganelo.com/manga/murabito_desu_ga_nani_ka)
- [MangaNelo - tensai shitara slime data ken](https://neroburner.gitlab.io/comics_to_rss/tensai-shitara-silme-datta-ken.xml) - [WebSite](https://manganelo.com/manga/dnha19771568647794)
- [MangaNelo - Iron Ladies](https://neroburner.gitlab.io/comics_to_rss/iron-ladies.xml) - [WebSite](https://manganelo.com/manga/jc917903)
- [MangaNelo - Berserk of Gluttony](https://neroburner.gitlab.io/comics_to_rss/berserk_of_gluttony.xml) - [WebSite](https://manganelo.com/manga/berserk_of_gluttony)
- [Manganato - Dungeon Reset](https://neroburner.gitlab.io/comics_to_rss/dungeon_reset.xml) - [WebSite](https://readmanganato.com/manga-hn984796/chapter-106)
- [Manganato - Onepunch-Man (One)](https://neroburner.gitlab.io/comics_to_rss/onepunchman_one.xml) - [WebSite](https://readmanganato.com/manga-rv952056)
- [Manganato - Onepunch-Man](https://neroburner.gitlab.io/comics_to_rss/onepunchman.xml) - [WebSite](https://readmanganato.com/manga-wd951838)
- [Manganato - The Strongest Florist](https://neroburner.gitlab.io/comics_to_rss/the-strongest-florist.xml) - [WebSite](https://readmanganato.com/manga-hp985172)
- [Manganato - The Player That Can't Level Up](https://neroburner.gitlab.io/comics_to_rss/player_cant_level_up.xml) - [WebSite](https://readmanganato.com/manga-kw988331)
- [Manganato - Tsuki Ga Michibiku Isekai Douchuu](https://neroburner.gitlab.io/comics_to_rss/tsuki_ga_michibiku.xml) - [WebSite](https://manganato.com/manga-rh953142)
- [Manganato - Boku No Hero Academia](https://neroburner.gitlab.io/comics_to_rss/boku_no_hero_academia.xml) - [WebSite](https://readmanganato.com/manga-jq951973)
- [Manganato - Versatile Mage](https://neroburner.gitlab.io/comics_to_rss/versatile_mage.xml) - [WebSite](https://readmanganato.com/manga-bf979214)
- [Manganato - Instant Death](https://neroburner.gitlab.io/comics_to_rss/instant_death.xml) - [WebSite](https://readmanganato.com/manga-bh979290)
